FROM debian:stable-slim
RUN apt-get update && apt dist-upgrade -y
RUN apt-get install -y eatmydata
RUN eatmydata apt-get install -y git gcc-arm-none-eabi make cmake python3 g++
